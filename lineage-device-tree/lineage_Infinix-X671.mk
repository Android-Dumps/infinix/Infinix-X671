#
# Copyright (C) 2023 The LineageOS Project
#
# SPDX-License-Identifier: Apache-2.0
#

# Inherit from those products. Most specific first.
$(call inherit-product, $(SRC_TARGET_DIR)/product/core_64_bit.mk)
$(call inherit-product, $(SRC_TARGET_DIR)/product/full_base_telephony.mk)

# Inherit some common Lineage stuff.
$(call inherit-product, vendor/lineage/config/common_full_phone.mk)

# Inherit from Infinix-X671 device
$(call inherit-product, device/infinix/Infinix-X671/device.mk)

PRODUCT_DEVICE := Infinix-X671
PRODUCT_NAME := lineage_Infinix-X671
PRODUCT_BRAND := Infinix
PRODUCT_MODEL := Infinix X671
PRODUCT_MANUFACTURER := infinix

PRODUCT_GMS_CLIENTID_BASE := android-transsion

PRODUCT_BUILD_PROP_OVERRIDES += \
    PRIVATE_BUILD_DESC="sys_tssi_64_armv82_infinix-user 12 SP1A.210812.016 308491 release-keys"

BUILD_FINGERPRINT := Infinix/X671-GL/Infinix-X671:12/SP1A.210812.016/220527V912:user/release-keys
